# Linked & Focused

This Firefox extension hides the feed from you LinkedIn page. This is useful if you use LinkedIn for your work and don't want to be distracted by the feed. 

## Install

Install from the official [Linked and Focused Firefox Add-ons page](https://addons.mozilla.org/en-US/firefox/addon/linked-and-focused/)

## Usage

Use the extension button in the Firefox toolbar to toogle the extension on and off. 
