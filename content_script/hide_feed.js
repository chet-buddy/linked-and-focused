let hideFeed = true;

browser.runtime.onMessage.addListener( (request) => {
	if(request.message === "hideFeedOff") {
	        hideFeed = false; 
	} else {
		hideFeed = true;
	}
});

const node = document;
const config = {attributes: true, attributeFilter: ["class"], subtree: true, childList: true};
const toBeRemoved = [
	".feed-follows-module", // "Add to your feed" side bar
	".feed-sort-toggle-dsa__wrapper", // "Sort by" option
	".scaffold-finite-scroll__load-button" //"Show more feed updates" button
]
const toBeHidden = [
	'[data-finite-scroll-hotkey-context="FEED"]' // Feed
]
const clearFeed = (node) => {
	toBeRemoved.forEach(div => node.querySelector(div)?.remove());
	toBeHidden.forEach(div => {
		let feed = node.querySelector(div)
		if (feed !== null) feed.hidden=true
	});
}

// Remove feed on page update
const callback = () => { 
	if (hideFeed) clearFeed(node);
}

const observer = new MutationObserver(callback);
observer.observe(node, config);
