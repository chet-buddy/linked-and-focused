const titleOn = "Linked & Focused";
const titleOff = "Linked & Focused (off)";

const run = (tab) => {
	browser.browserAction.getTitle({tabId: tab.id}).then( (title) => {
		if (title === titleOn) {
			browser.browserAction.setIcon({tabId: tab.id, path: "icons/off.png"});
			browser.browserAction.setTitle({tabId: tab.id, title: titleOff});
			browser.tabs.sendMessage(tab.id, {message: "hideFeedOff"});
		} else {
			browser.browserAction.setIcon({tabId: tab.id, path: "icons/on.png"});
			browser.browserAction.setTitle({tabId: tab.id, title: titleOn});
			browser.tabs.sendMessage(tab.id, {message: "hideFeedOn"});
		}
	});
}

browser.browserAction.onClicked.addListener(run);
